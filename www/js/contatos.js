document.addEventListener("deviceready", onDeviceReady, false);

function onDeviceReady() {
	
	function sucessoAoSalvarContato(contato) {
		alert("Salvo com sucesso!");
	}
	
	function erroAoSalvarContato(erro) {
    alert("Erro: " + erro.code);
  }

	$("#salvarContato").bind("click", function(event){
		var nome = $("#nome").val();
		var sobrenome = $("#sobrenome").val();
		var contato = navigator.contacts.create();
		contato.displayName = nome;
		contato.nickname = nome;   
		contato.name = new ContactName();
		contato.name.givenName = nome;
		contato.name.familyName = sobrenome;
		contato.save(sucessoAoSalvarContato, erroAoSalvarContato);
	});
	
	function sucessoAoRemoverContato(contato) {
		alert("Removido com sucesso!");
	}
	
	function erroAoRemoverContato(erro) {
    alert("Erro: " + erro.code);
  }
	
	function removerContato(id){
		var contato = navigator.contacts.create();
		contato.id = id;
		contato.remove(sucessoAoRemoverContato, erroAoRemoverContato);
	}
	
	function sucessoAoListarContatos(contatos) {
		var html = "<ul>";
		if (contatos.length > 0){
			for (var i=0; i<contatos.length; i++) {
				var id = contatos[i].id;
				var nome = contatos[i].name.givenName;
				var sobrenome = contatos[i].name.familyName;
				if (nome != null && sobrenome != null){
				 html += "<li data-icon=false id='" + id + "'>";		  
				 html +=    "<a href='#'>";		  
				 html +=       nome + "&nbsp;" + sobrenome;		  
				 html +=    "</a>";		  
				 html += "</li>";	
				}
      }
		} else {
		  html += "<li>Nenhum contato cadastrado.</li>";
		}
		html += "</ul>";
		
		var $content = $("#paginaListar div:jqmData(role=content)");
		$content.html(html);  
		var $ul = $content.find("ul");
		$ul.listview();
		$("li").bind("taphold", function(event){
		 var id = $(this).attr("id");	
		 if(!id) return;
		 if (!confirm("Remover contato?", "Alerta")) return;			 
		 $(this).remove();	  
		 removerContato(id);
		});		

		$.mobile.changePage("#paginaListar", { transition: "none" });
  }
	
	function erroAoListarContatos(erro) {
    alert('Erro: ' + erro.code);
  }
	
	$("#btnListar").bind("click", function(event){
	   var filtro = new ContactFindOptions();
     filtro.filter=""; 
	   filtro.multiple=true;
     var campos = ["name"];
     navigator.contacts.find(campos, sucessoAoListarContatos, erroAoListarContatos, filtro);
	});
}